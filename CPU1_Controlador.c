//Trabalho III - C�digo do Controlador
//T�cnicas de Comando e Instrumenta��o em Eletr�nica de Pot�ncia
//P�s-Gradua��o em Engenharia El�trica
//Universidade Federal de Minas Gerais
//Autores: Andr� Lage Almeida Dias e Yasmine Neves Maia

#include "F28x_Project.h"                      // Device Header File and Examples Include File
#include "math.h"

// Function Prototypes
void ConfigureADC(void);
void ConfigureDAC(void);
void SetupADCEpwm(void);
void InitGPIO(void);
void InitEPwms(void);                          // Configure ePWM modules
void InitEPwm8_Temporizador_Interrupcao(void); // Configure ePWM module 8 (timer interrupt)
interrupt void Interrupcao(void);              // ADC interrupt service routine
interrupt void Receber_da_CPU2(void);

// Variables
Uint16 periodo = 400;                          // PWMs freq. = 250kHz -> Triangular Up -> T = 200MHz / 250kHz -> T/2 = 800/2 = 400
float seno_table[180];
float buffer[240], buffer2[250];
Uint16 indice=0;

//Vari�veis da m�quina de estados
Uint16 INIT=0, INIT_OK=0;
Uint16 ON_OFF=0;
Uint16 ERROR=0;
Uint16 RESET=0;

// Controlador PI
float wt=0, wt_b=0, wt_c=0;
int theta_a=0, theta_b=0, theta_c=0;
float Ts=200e-6, Im=0;
float ia=0, ib=0, ic=0, iah=0, ibh=0;
float ia_ref=0, ib_ref=0, ic_ref=0;
float ialpha=0, ibeta=0;
float outa=0, outb=0, outc=0, out_pwma=0, out_pwmb=0, out_pwmc=0;

float iref_a=0, erro_a=0, erro_ant_a=0, P_a=0, I_a=0, PI_a=0, PIR_a=0;
float iref_b=0, erro_b=0, erro_ant_b=0, P_b=0, I_b=0, PI_b=0, PIR_b=0;
float erro_c=0, P_c=0, I_c=0, PI_c=0;
float kp=0.0141, ki=25.8438;
Uint16 out_daca=0, out_dacb=0, ADCINA0=0, ADCINA1=0;
Uint16 i=0, k=0, flag=0;

// Controlador Ressonante em 60Hz
float x_a=0, x1_a=0, x2_a=0, y_a=0, y1_a=0, y2_a=0;
float x_b=0, x1_b=0, x2_b=0, y_b=0, y1_b=0, y2_b=0;
float b0 = 0.0012885493;
float b1 = 0;
float b2 = -0.0012885493;
float a1 = -1.9918239;
float a2 = 0.99749271;

void main(void)
{
   // Initialize System Control
 InitSysCtrl();
   EALLOW;
   ClkCfgRegs.PERCLKDIVSEL.bit.EPWMCLKDIV = 0; // Divide o Clock principal por 1. -> 200MHz / 1 = 200MHz
   EDIS;

   // Initialize GPIO
   InitGpio();                                 // Configure default GPIO
   InitEPwm1Gpio();                            // Configure EPWM1 GPIO pins
   InitEPwm2Gpio();                            // Configure EPWM2 GPIO pins
   InitEPwm3Gpio();                            // Configure EPWM3 GPIO pins

   // Clear all interrupts and initialize PIE vector table
   DINT;
   InitPieCtrl();
   IER = 0x0000;
   IFR = 0x0000;
   InitPieVectTable();

   // Map ISR functions
   EALLOW;
   PieVectTable.ADCA1_INT = &Interrupcao;      // Function for ADCA interrupt 1

   PieVectTable.IPC0_INT = &Receber_da_CPU2;      // IPC1 interrupt
   EDIS;
   
   // OBS.: A TABELA DE SENOS FOI DECLARADA MANUALMENTE PORQUE USANDO UM LA�O FOR DEU ERRO, ALGUNS VALORES ERAM SIMPLESMNTE "ZERADOS" SEM MOTIVO EXPL�CITO.
   seno_table[0] = 0.0;
   seno_table[1] = 0.035094386; seno_table[2] = 0.070145536; seno_table[3] = 0.105110267; seno_table[4] = 0.139945503; seno_table[5] = 0.174608326; seno_table[6] = 0.209056033;
   seno_table[7] = 0.243246184; seno_table[8] = 0.277136656; seno_table[9] = 0.310685698; seno_table[10] = 0.343851976; seno_table[11] = 0.37659463; seno_table[12] = 0.408873321;
   seno_table[13] = 0.440648282; seno_table[14] = 0.471880366; seno_table[15] = 0.502531097; seno_table[16] = 0.532562711; seno_table[17] = 0.56193821; seno_table[18] = 0.590621404;
   seno_table[19] = 0.618576955; seno_table[20] = 0.645770422; seno_table[21] = 0.672168303; seno_table[22] = 0.697738076; seno_table[23] = 0.72244823; seno_table[24] = 0.746268348;
   seno_table[25] = 0.769169059; seno_table[26] = 0.791122157; seno_table[27] = 0.812100596; seno_table[28] = 0.832078531; seno_table[29] = 0.851031349; seno_table[30] = 0.8689357;
   seno_table[31] = 0.885769526; seno_table[32] = 0.901512089; seno_table[33] = 0.916143992; seno_table[34] = 0.929647211; seno_table[35] = 0.942005108; seno_table[36] = 0.95320246;
   seno_table[37] = 0.96322547; seno_table[38] = 0.972061792; seno_table[39] = 0.979700537; seno_table[40] = 0.986132296; seno_table[41] = 0.991349145; seno_table[42] = 0.995344656;
   seno_table[43] = 0.998113907; seno_table[44] = 0.999653486; seno_table[45] = 0.999961496; seno_table[46] = 0.999037559; seno_table[47] = 0.996882812; seno_table[48] = 0.99349991;
   seno_table[49] = 0.988893021; seno_table[50] = 0.98306782; seno_table[51] = 0.976031484; seno_table[52] = 0.967792682; seno_table[53] = 0.958361564; seno_table[54] = 0.947749749;
   seno_table[55] = 0.93597031; seno_table[56] = 0.92303776; seno_table[57] = 0.908968032; seno_table[58] = 0.89377846; seno_table[59] =  0.877487756; seno_table[60] = 0.860115992;
   seno_table[61] = 0.841684569; seno_table[62] = 0.822216194; seno_table[63] = 0.801734853; seno_table[64] = 0.780265778; seno_table[65] = 0.757835419; seno_table[66] = 0.734471411;
   seno_table[67] = 0.710202537; seno_table[68] = 0.685058697; seno_table[69] = 0.659070867; seno_table[70] = 0.632271066; seno_table[71] = 0.604692309; seno_table[72] = 0.576368575;
   seno_table[73] = 0.547334756; seno_table[74] = 0.517626624; seno_table[75] = 0.487280779; seno_table[76] = 0.456334605; seno_table[77] = 0.42482623; seno_table[78] = 0.39279447;
   seno_table[79] = 0.36027879; seno_table[80] = 0.327319248; seno_table[81] = 0.29395645; seno_table[82] = 0.260231499; seno_table[83] = 0.226185944; seno_table[84] = 0.191861729;
   seno_table[85] = 0.157301141; seno_table[86] = 0.122546759; seno_table[87] = 0.0876414; seno_table[88] = 0.052628067; seno_table[89] = 0.017549896;

   seno_table[90] = -0.017549895;
   seno_table[91] = -0.052628066; seno_table[92] = -0.087641399; seno_table[93] = -0.122546758; seno_table[94] = -0.15730114; seno_table[95] = -0.191861728; seno_table[96] = -0.226185943;
   seno_table[97] = -0.260231498; seno_table[98] = -0.293956449; seno_table[99] = -0.327319247; seno_table[100] = -0.360278789; seno_table[101] = -0.39279447; seno_table[102] = -0.424826229;
   seno_table[103] = -0.456334605; seno_table[104] = -0.487280778; seno_table[105] = -0.517626624; seno_table[106] = -0.547334756; seno_table[107] = -0.576368574; seno_table[108] = -0.604692309;
   seno_table[109] = -0.632271065; seno_table[110] = -0.659070867; seno_table[111] = -0.685058696; seno_table[112] = -0.710202536; seno_table[113] = -0.73447141; seno_table[114] = -0.757835419;
   seno_table[115] = -0.780265777; seno_table[116] = -0.801734852; seno_table[117] = -0.822216194; seno_table[118] = -0.841684568; seno_table[119] = -0.860115992; seno_table[120] = -0.877487756;
   seno_table[121] = -0.893778459; seno_table[122] = -0.908968032; seno_table[123] = -0.92303776; seno_table[124] = -0.93597031; seno_table[125] = -0.947749748; seno_table[126] = -0.958361564;
   seno_table[127] = -0.967792682; seno_table[128] = -0.976031484; seno_table[129] = -0.98306782; seno_table[130] = -0.988893021; seno_table[131] = -0.99349991; seno_table[132] = -0.996882812;
   seno_table[133] = -0.999037559; seno_table[134] = -0.999961496; seno_table[135] = -0.999653486; seno_table[136] = -0.998113907; seno_table[137] = -0.995344656; seno_table[138] = -0.991349145;
   seno_table[139] = -0.986132296; seno_table[140] = -0.979700537; seno_table[141] = -0.972061792; seno_table[142] = -0.963225471; seno_table[143] = -0.95320246; seno_table[144] = -0.942005109;
   seno_table[145] = -0.929647211; seno_table[146] = -0.916143993; seno_table[147] = -0.901512089; seno_table[148] = -0.885769527; seno_table[149] = -0.8689357; seno_table[150] = -0.851031349;
   seno_table[151] = -0.832078531; seno_table[152] = -0.812100596; seno_table[153] = -0.791122157; seno_table[154] = -0.76916906; seno_table[155] = -0.746268349; seno_table[156] = -0.722448239;
   seno_table[157] = -0.697738077; seno_table[158] = -0.672168304; seno_table[159] = -0.645770423; seno_table[160] = -0.618576956; seno_table[161] = -0.590621405; seno_table[162] = -0.561938211;
   seno_table[163] = -0.532562711; seno_table[164] = -0.502531097; seno_table[165] = -0.471880367; seno_table[166] = -0.440648283; seno_table[167] = -0.408873322; seno_table[168] = -0.37659463;
   seno_table[169] = -0.343851976; seno_table[170] = -0.310685698; seno_table[171] = -0.277136657; seno_table[172] = -0.243246184; seno_table[173] = -0.209056034; seno_table[174] = -0.174608327;
   seno_table[175] = -0.139945504; seno_table[176] = -0.105110268; seno_table[177] = -0.070145537; seno_table[178] = -0.035094387; seno_table[179] = 0.0;


   IpcRegs.IPCCLR.all = 0xFFFFFFFF;

   // Enable global interrupts and higher priority real-time debug events
   IER |= M_INT1;                              // Enable group 1 interrupts
   IER |= M_INT2;                              // Enable group 2 interrupts
   EINT;                                       // Enable Global interrupt INTM
   ERTM;                                       // Enable Global real time interrupt DBGM

   // Enable PIE interrupt
   PieCtrlRegs.PIEIER1.bit.INTx1 = 1;

   PieCtrlRegs.PIEIER1.bit.INTx13 = 1;      // IPC1 ISR

   PieCtrlRegs.PIEIER2.bit.INTx1 = 1;
   PieCtrlRegs.PIEIER2.bit.INTx2 = 1;

   // Esperar at� que a CPU02 esteja pronta
   while (IpcRegs.IPCSTS.bit.IPC17 == 0) ; // Wait for CPU02 to set IPC17
   IpcRegs.IPCACK.bit.IPC17 = 1;           // Acknowledge and clear IPC17

   // Sync ePWM
   EALLOW;
   CpuSysRegs.PCLKCR0.bit.TBCLKSYNC = 1;
   EDIS;


   while(true) {

       // A m�quina de estados do sistema � executada no loop infinito

            /* Caso o conversor/simulador ainda n�o tenham sido inicializados, o comando INIT = 1 executa a inicializa��o do sistema,
               para isso o conversor deve estar desligado (ON_OFF = 0) */

            if(INIT && !ON_OFF && !INIT_OK)
            {
               // Configure the ADC and power it up
               ConfigureADC();
               // Configure the DAC
               ConfigureDAC();
               // Setup the ADC for ePWM triggered conversions on channel 0
               SetupADCEpwm();
               // Setup GPIO IN/OUT
               InitGPIO();
               // Initialize ePWM modules
               InitEPwms();
               // Initialize timer of interrupt
               InitEPwm8_Temporizador_Interrupcao();
               // Confirma inicializa��o
               INIT_OK = 1;
            }

            /* O comando ON_OFF = 1 liga o conversor/simulador, e ON_OFF = 0 desliga
                    Para ligar � necess�rio que:
                    A inicializa��o j� tenha sido realizada (flag INIT_OK = 1);
                    O sistema n�o esteja com registro de falha (ERROR = 0);
                    O comando RESET esteja inativo (RESET = 0). */

       if(ON_OFF && INIT_OK && !ERROR && !RESET)
       {
         // Permite a contagem do timer/PWM8 que sincroniza o evento de interrup��o do conversor/simulador
         EPwm8Regs.TBPRD = periodo;
        }
       else
       {
       //Impossibilita a contagem do timer/PWM que sincroniza o evento de interrup��o do modelo/controlador
       EPwm8Regs.TBPRD = 0;

        // Reinicializa todas as vari�veis do controle
         wt=0; wt_b=0; wt_c=0;
         theta_a=0; theta_b=0; theta_c=0;
         Im=0; ia=0; ib=0; ic=0; iah=0; ibh=0;
         ia_ref=0; ib_ref=0; ic_ref=0;
         ialpha=0; ibeta=0;
         outa=0; outb=0; outc=0; out_pwma=0; out_pwmb=0; out_pwmc=0;
         iref_a=0; erro_a=0; erro_ant_a=0; P_a=0; I_a=0; PI_a=0; PIR_a=0;
         iref_b=0; erro_b=0; erro_ant_b=0; P_b=0; I_b=0; PI_b=0; PIR_b=0;
         out_daca=0; out_dacb=0; ADCINA0=0, ADCINA1=0;
         i=0; k=0; flag=0;
         x_a=0; x1_a=0; x2_a=0; y_a=0; y1_a=0; y2_a=0;
         x_b=0; x1_b=0; x2_b=0; y_b=0; y1_b=0; y2_b=0;

         // Passa todos os PWMs iguais a zero para "limpar" o valor das correntes na CPU2
         IpcRegs.IPCSENDDATA = (Uint16) 0;
         IpcRegs.IPCSENDCOM = (Uint16) 0;
         IpcRegs.IPCSENDADDR = (Uint16) 0;
         IpcRegs.IPCSET.bit.IPC1 = 1;
       }

       /* O comando RESET = 1 possibilita uma nova partida do conversor/simulador ap�s restabelecida a causa/condi��o de falha (sobrecorrente),
                       para aplicar o comando RESET = 1 � necess�rio que o conversor esteja em modo off (ON_OFF = 0) */

       if(RESET && !ON_OFF)
         {
          ERROR = 0;
         }


   }
}

// Write ADCs configurations and power up the ADCs
void ConfigureADC(void)
{
   EALLOW;

   // ADC-A
   AdcaRegs.ADCCTL2.bit.PRESCALE = 6;          // Set ADCCLK divider to /4
   AdcaRegs.ADCCTL2.bit.RESOLUTION =  0;       // 12-bit resolution
   AdcaRegs.ADCCTL2.bit.SIGNALMODE = 0;        // Single-ended channel conversions (12-bit mode only)
   AdcaRegs.ADCCTL1.bit.INTPULSEPOS = 1;       // Set pulse positions to late
   AdcaRegs.ADCCTL1.bit.ADCPWDNZ = 1;          // Power up the ADC

   // ADC-B
   AdcbRegs.ADCCTL2.bit.PRESCALE = 6;          // Set ADCCLK divider to /4
   AdcbRegs.ADCCTL2.bit.RESOLUTION =  0;       // 12-bit resolution
   AdcbRegs.ADCCTL2.bit.SIGNALMODE = 0;        // Single-ended channel conversions (12-bit mode only)
   AdcbRegs.ADCCTL1.bit.INTPULSEPOS = 1;       // Set pulse positions to late
   AdcbRegs.ADCCTL1.bit.ADCPWDNZ = 1;          // Power up the ADC

   DELAY_US(1000);                             // Delay for 1ms to allow ADC time to power up
   EDIS;
}

void ConfigureDAC(void)
{
   EALLOW;
   DacaRegs.DACCTL.bit.DACREFSEL = 1;          // Use ADC references
   DacaRegs.DACCTL.bit.LOADMODE = 0;           // Load on next SYSCLK
   DacaRegs.DACVALS.all = 0x0000;              // Set mid-range
   DacaRegs.DACOUTEN.bit.DACOUTEN = 1;         // Enable DAC

   DacbRegs.DACCTL.bit.DACREFSEL = 1;          // Use ADC references
   DacbRegs.DACCTL.bit.LOADMODE = 0;           // Load on next SYSCLK
   DacbRegs.DACVALS.all = 0x0000;              // Set mid-range
   DacbRegs.DACOUTEN.bit.DACOUTEN = 1;         // Enable DAC
   EDIS;
}
void SetupADCEpwm(void)
{
   // Select the channels to convert and end of conversion flag
   EALLOW;

   // ADC-A
   AdcaRegs.ADCINTSEL1N2.bit.INT1SEL = 0;      // End of SOC0 will set INT1 flag
   AdcaRegs.ADCINTSEL1N2.bit.INT1E = 1;        // Enable INT1 flag
   AdcaRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;      // Make sure INT1 flag is cleared

   AdcaRegs.ADCSOC0CTL.bit.CHSEL = 0;          //SOC0 will convert ADCINA0
   AdcaRegs.ADCSOC0CTL.bit.ACQPS = 14;         //SOC0 will use sample duration of 100 SYSCLK cycles -> 1us
   AdcaRegs.ADCSOC0CTL.bit.TRIGSEL = 0x0013;   //SOC0 will begin conversion Trigger on ePWM8

   AdcaRegs.ADCSOC1CTL.bit.CHSEL = 1;          //SOC1 will convert ADCINA1
   AdcaRegs.ADCSOC1CTL.bit.ACQPS = 14;         //SOC1 will use sample duration of 100 SYSCLK cycles
   AdcaRegs.ADCSOC1CTL.bit.TRIGSEL = 0x0013;   //SOC1 will begin conversion Trigger on ePWM8

   EDIS;
}

void InitGPIO(void)
{
   EALLOW;

   GpioCtrlRegs.GPADIR.bit.GPIO31 = 1;         // Pino LED
   GpioCtrlRegs.GPADIR.bit.GPIO18 = 1;         // Sinalizador de in�cio/fim da INT.
   GpioCtrlRegs.GPADIR.bit.GPIO19 = 1;         // Sinalizador de in�cio/fim do controle subamostrado.
   GpioCtrlRegs.GPADIR.bit.GPIO24 = 0;         // 0 -> INPUT, 1 -> OUTPUT
   GpioCtrlRegs.GPADIR.bit.GPIO16 = 0;         // 0 -> INPUT, 1 -> OUTPUT
   GpioCtrlRegs.GPADIR.bit.GPIO22 = 0;         // 0 -> INPUT, 1 -> OUTPUT

   GpioCtrlRegs.GPAPUD.bit.GPIO24 = 0;         // Ativar Pull up no GPIO24
   GpioCtrlRegs.GPAPUD.bit.GPIO16 = 0;         // Ativar Pull up no GPIO16
   GpioCtrlRegs.GPAPUD.bit.GPIO22 = 0;         // Ativar Pull up no GPIO22

   EDIS;

   GpioDataRegs.GPADAT.bit.GPIO31 = 1;         // Turn off LED
   GpioDataRegs.GPADAT.bit.GPIO18 = 0;         // Saida para monitorar interrup��o
   GpioDataRegs.GPADAT.bit.GPIO19 = 0;         // Saida para monitorar controlador
}

void InitEPwms(void)
{
   // PWM1
   EPwm1Regs.TBCTL.bit.CTRMODE = 2;            // Count up-down
   EPwm1Regs.TBPRD = periodo*50;              // Set timer period
   EPwm1Regs.TBCTL.bit.PHSEN = 0;              // Enable phase loading
   EPwm1Regs.TBPHS.bit.TBPHS = 0x0000;         // Phase is 0
   EPwm1Regs.TBCTR = 0x0000;                   // Clear counter
   EPwm1Regs.TBCTL.bit.HSPCLKDIV = 0;          // Clock ratio to SYSCLKOUT
   EPwm1Regs.TBCTL.bit.CLKDIV = 0;
   EPwm1Regs.TBCTL.bit.SYNCOSEL = 1;           // SYNC output on CTR = 0
   // Setup shadow register load on ZERO
   EPwm1Regs.CMPCTL.bit.SHDWAMODE = 0;
   EPwm1Regs.CMPCTL.bit.SHDWBMODE = 0;
   EPwm1Regs.CMPCTL.bit.LOADAMODE = 0;
   EPwm1Regs.CMPCTL.bit.LOADBMODE = 0;
   // Set Compare values
   EPwm1Regs.CMPA.bit.CMPA = 0;        // Set compare A value 0% initial
   EPwm1Regs.AQCTLA.bit.CAU = 2;               // Set: force EPWMxA output high.
   EPwm1Regs.AQCTLA.bit.CAD = 1;               // Clear: force EPWMxA output low.

   // PWM2
   EPwm2Regs.TBCTL.bit.CTRMODE = 2;            // Count up-down
   EPwm2Regs.TBPRD = periodo*50;              // Set timer period
   EPwm2Regs.TBCTL.bit.PHSEN = 1;              // Enable phase loading
   EPwm2Regs.TBPHS.bit.TBPHS = 0x0000;         // Phase is 0
   EPwm2Regs.TBCTR = 0x0000;                   // Clear counter
   EPwm2Regs.TBCTL.bit.HSPCLKDIV = 0;          // Clock ratio to SYSCLKOUT
   EPwm2Regs.TBCTL.bit.CLKDIV = 0;
   EPwm2Regs.TBCTL.bit.SYNCOSEL = 1;           // SYNC output on CTR = 0
   // Setup shadow register load on ZERO
   EPwm2Regs.CMPCTL.bit.SHDWAMODE = 0;
   EPwm2Regs.CMPCTL.bit.SHDWBMODE = 0;
   EPwm2Regs.CMPCTL.bit.LOADAMODE = 0;
   EPwm2Regs.CMPCTL.bit.LOADBMODE = 0;
   // Set Compare values
   EPwm2Regs.CMPA.bit.CMPA = 0;        // Set compare A value 0% initial
   EPwm2Regs.AQCTLA.bit.CAU = 2;               // Set: force EPWMxA output high.
   EPwm2Regs.AQCTLA.bit.CAD = 1;               // Clear: force EPWMxA output low.

   // PWM3
   EPwm3Regs.TBCTL.bit.CTRMODE = 2;            // Count up-down
   EPwm3Regs.TBPRD = periodo*50;              // Set timer period
   EPwm3Regs.TBCTL.bit.PHSEN = 1;              // Enable phase loading
   EPwm3Regs.TBPHS.bit.TBPHS = 0x0000;         // Phase is 0
   EPwm3Regs.TBCTR = 0x0000;                   // Clear counter
   EPwm3Regs.TBCTL.bit.HSPCLKDIV = 0;          // Clock ratio to SYSCLKOUT
   EPwm3Regs.TBCTL.bit.CLKDIV = 0;
   EPwm3Regs.TBCTL.bit.SYNCOSEL = 1;           // SYNC output on CTR = 0
   // Setup shadow register load on ZERO
   EPwm3Regs.CMPCTL.bit.SHDWAMODE = 0;
   EPwm3Regs.CMPCTL.bit.SHDWBMODE = 0;
   EPwm3Regs.CMPCTL.bit.LOADAMODE = 0;
   EPwm3Regs.CMPCTL.bit.LOADBMODE = 0;
   // Set Compare values
   EPwm3Regs.CMPA.bit.CMPA = 0;        // Set compare A value 0% initial
   EPwm3Regs.AQCTLA.bit.CAU = 2;               // Set: force EPWMxA output high.
   EPwm3Regs.AQCTLA.bit.CAD = 1;               // Clear: force EPWMxA output low.
}

void InitEPwm8_Temporizador_Interrupcao(void)
{
   EALLOW;
   // Assumes ePWM clock is already enabled
   EPwm8Regs.TBCTL.bit.CTRMODE = 2;            // 0 -> UP, 1 -> DOWN, 2 -> UP/DOWN, 3 -> CONGELA
   EPwm8Regs.TBCTL.bit.HSPCLKDIV = 0;          // TBCLK pre-scaler = /1
   EPwm8Regs.TBCTL.bit.CLKDIV = 0;             // TBCLK pre-scaler = /1
   EPwm8Regs.TBPRD =0;                  // Set period
   EPwm8Regs.TBCTL.bit.PHSEN = 1;              // Enable phase loading
   EPwm8Regs.TBPHS.bit.TBPHS = 0x0000;         // Phase is 0
   EPwm8Regs.ETSEL.bit.SOCAEN  = 0;            // Disable SOC on A group
   EPwm8Regs.ETSEL.bit.SOCASEL = 2;            // Enable event time-base counter equal to period (TBCTR = TBPRD)
   EPwm8Regs.ETSEL.bit.SOCAEN = 1;             // Enable SOCA
   EPwm8Regs.ETPS.bit.SOCAPRD = 1;             // Generate pulse on 1st event
   EDIS;
}

interrupt void Interrupcao(void)
{
   GpioDataRegs.GPADAT.bit.GPIO18 = 1;

   // =============== Enviar PWMs para a CPU2 executar o modelo do HIL

   IpcRegs.IPCSENDDATA = (Uint16) GpioDataRegs.GPADAT.bit.GPIO24;
   IpcRegs.IPCSENDCOM = (Uint16) GpioDataRegs.GPADAT.bit.GPIO16;
   IpcRegs.IPCSENDADDR = (Uint16) GpioDataRegs.GPADAT.bit.GPIO22;
   IpcRegs.IPCSET.bit.IPC1 = 1;

   // =============== Gera��o do �ngulo das refer�ncias senoidais com f = 60Hz

   wt = k*0.0015;
   flag = 1;
   if(wt >= 6.28318){
      wt -= 6.28318;
      k=0;
      flag = 0;
   }
   if(flag==1) k++;

   // =============== In�cio do controlador -> Subamostragem de 250kHz para 5kHz

   i++;
   if(i==50){
   i=0;

   GpioDataRegs.GPADAT.bit.GPIO19 = 1;

   // =============== Oscilosc�pio

      buffer[indice] = ia;
      buffer2[indice] = ib;
      indice++; if(indice>249) indice = 0;

   // =============== Valor de pico da refer�ncia aplicado em rampa com dura��o de 10 segundos.

   if(Im<10) Im += Ts;

   // =============== Gera��o das refer�ncias senoidais

   wt_b = (wt - 2.094395); if(wt_b < 0.0) wt_b += 6.28318;
   wt_c = (wt + 2.094395); if(wt_c > 6.28318) wt_c -= 6.28318;

   theta_a = wt*28.4887;
   theta_b = wt_b*28.4887;
   theta_c = wt_c*28.4887;

   ia_ref = Im*seno_table[theta_a];
   ib_ref = Im*seno_table[theta_b];
   ic_ref = Im*seno_table[theta_c];

   // =============== Leitura das entradas

   ADCINA0 = AdcaResultRegs.ADCRESULT0;
   ADCINA1 = AdcaResultRegs.ADCRESULT1;

   ia = (ADCINA0 - 1861.0) * 0.01;
   ib = (ADCINA1 - 1861.0) * 0.01;
   ic = -(ia + ib);
   
   // =============== Monitor de sobrecorrentes / falhas

   if( ia>17||ia<-17 || ib>17||ib<-17 || ic>17||ic<-17 ) ERROR = 1;

   // =============== Transforma��es abc / alpha beta

   iref_a = 0.8164965*(ia_ref - 0.5*ib_ref - 0.5*ic_ref);
   iref_b = 0.8164965*(0.8660254*ib_ref - 0.8660254*ic_ref);

   ialpha = 0.8164965*(ia - 0.5*ib - 0.5*ic);
   ibeta = 0.8164965*(0.8660254*ib - 0.8660254*ic);

   // =============== PI alpha/beta

   erro_a = iref_a - ialpha;
   P_a = kp* erro_a;
   I_a +=  ki*0.5*Ts*(erro_a + erro_ant_a);
   erro_ant_a = erro_a;
   PI_a = P_a + I_a;

   erro_b = iref_b - ibeta;
   P_b = kp* erro_b;
   I_b +=  ki*0.5*Ts*(erro_b + erro_ant_b);
   erro_ant_b = erro_b;
   PI_b = P_b + I_b;

   // =============== Ressonantes em 60Hz

   x_a = erro_a;
   y_a = b0*x_a + b1*x1_a + b2*x2_a - a1*y1_a - a2*y2_a;
   y2_a = y1_a;
   y1_a = y_a;
   x2_a = x1_a;
   x1_a = x_a;
   PIR_a = PI_a + y_a;

   x_b = erro_b;
   y_b = b0*x_b + b1*x1_b + b2*x2_b - a1*y1_b - a2*y2_b;
   y2_b = y1_b;
   y1_b = y_b;
   x2_b = x1_b;
   x1_b = x_b;
   PIR_b = PI_b + y_b;

   // =============== Transforma��es alpha beta / abc

   outa = 0.8164965*PIR_a;
   outb = 0.8164965*(-0.5*PIR_a + 0.8660254*PIR_b);
   outc = 0.8164965*(-0.5*PIR_a - 0.8660254*PIR_b);

   // =============== PWM

   out_pwma = (outa + 1.0)*0.5;
   if(out_pwma<0) out_pwma = 0; if(out_pwma>1) out_pwma = 1;

   out_pwmb = (outb + 1.0)*0.5;
   if(out_pwmb<0) out_pwmb = 0; if(out_pwmb>1) out_pwmb = 1;

   out_pwmc = (outc + 1.0)*0.5;
   if(out_pwmc<0) out_pwmc = 0; if(out_pwmc>1) out_pwmc = 1;

   EPwm1Regs.CMPA.bit.CMPA = (1.0 - out_pwma)*20000;
   EPwm2Regs.CMPA.bit.CMPA = (1.0 - out_pwmb)*20000;
   EPwm3Regs.CMPA.bit.CMPA = (1.0 - out_pwmc)*20000;

   GpioDataRegs.GPADAT.bit.GPIO19 = 0;

   }

   // Return from interrupt
   AdcaRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;      // Clear ADC INT1 flag
   PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;     // Acknowledge PIE group 1 to enable further interrupts

   GpioDataRegs.GPADAT.bit.GPIO18 = 0;
}

interrupt void Receber_da_CPU2(void){

    // =============== Recebe as correntes ia e ib calculadas pelo modelo executado na CPU2

    iah = (int) IpcRegs.IPCRECVDATA * 0.01;
    ibh = (int) IpcRegs.IPCRECVCOM * 0.01;

    // =============== Escreve a corrente ia no DAC-A (j� conectado internamente ao ADCINA0)

    out_daca = 100*iah + 1861.0;
    if(out_daca > 3723.0) out_daca = 3723.0; if(out_daca < 0.0) out_daca = 0;
    DacaRegs.DACVALS.all = out_daca;

    // =============== Escreve a corrente ib no DAC-B (j� conectado internamente ao ADCINA1)

    out_dacb = 100*ibh + 1861.0;
    if(out_dacb > 3723.0) out_dacb = 3723.0; if(out_dacb < 0.0) out_dacb = 0;
    DacbRegs.DACVALS.all = out_dacb;

    IpcRegs.IPCACK.bit.IPC0 = 1;                // Clear IPC1 bit
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;     // Acknowledge PIE group 1 to enable further interrupts
}
